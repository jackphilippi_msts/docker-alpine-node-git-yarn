# alpine-node-git-yarn

Forked from [jwigley/alpine-node-git-yarn](https://github.com/jwigley/docker-alpine-node-git-yarn)

A lightweight [alpine linux](https://alpinelinux.org) docker image containing node, yarn and git.

## usage

```bash
docker run -t jphilippimsts/alpine-node-git-yarn
```

## tags

| **Tag**      | **Description**                   |
| ------------ | --------------------------------- |
| `latest`     | latest alpine, node, git and yarn |
| `node-12.13` | alpine, node 12.13, git and yarn  |
